import { NgStmeterPage } from './app.po';

describe('ng-stmeter App', () => {
  let page: NgStmeterPage;

  beforeEach(() => {
    page = new NgStmeterPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
