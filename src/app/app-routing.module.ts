import { AcceptInviteComponent } from './auth/accept-invite/accept-invite.component';
import { UpdatePasswordComponent } from './auth/update-password/update-password.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesComponent } from './sales/sales.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SettingsComponent } from './settings/settings.component';
import { AuthGardService } from './auth/auth-gard.service';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'sales', pathMatch: 'full' },
  { path: 'authcallback', component: AuthCallbackComponent },
  { path: 'sales', component: SalesComponent, canActivate: [AuthGardService] },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGardService] },
  { path: 'signup', component: SignupComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'forgotPassword', component: ForgotPasswordComponent },
  { path: 'updatePassword', component: UpdatePasswordComponent },
  { path: 'accept-invite', component: AcceptInviteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
