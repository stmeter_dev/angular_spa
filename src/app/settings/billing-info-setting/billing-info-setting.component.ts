import { Component } from '@angular/core';

@Component({
    selector: 'app-billing-info-setting',
    templateUrl: 'billing-info-setting.component.html',
    styleUrls: ['billing-info-setting.component.scss']
})
export class BillingInfoSettingComponent {

    soloPlanPrice: number = 40;
    starterPlanPrice: number = 70;
    growthPlanPrice: number = 90;
    discountAmount: number = 20;
    discount: number;
    temp: number = this.soloPlanPrice;
    soloWithoutDiscount: number = this.soloPlanPrice;
    starterWithoutDiscount: number = this.starterPlanPrice;
    growthWithoutDiscount: number = this.growthPlanPrice;
    annualPlan: boolean = false;

    onAnnualPlan(plan) {
        if (plan === true) {
            this.annualPlan = true;
            //solo
            this.discount = 0;
            this.temp = this.soloWithoutDiscount;
            this.discount = ((this.temp * this.discountAmount) / 100);
            this.temp = (this.temp - this.discount);
            this.soloPlanPrice = this.temp;

            // starter
            this.discount = 0;
            this.temp = this.starterWithoutDiscount;
            this.discount = ((this.starterPlanPrice * this.discountAmount) / 100);
            this.temp = (this.starterPlanPrice - this.discount);
            this.starterPlanPrice = this.temp;

            // growth
            this.discount = 0;
            this.temp = this.growthWithoutDiscount;
            this.discount = ((this.growthPlanPrice * this.discountAmount) / 100);
            this.temp = (this.growthPlanPrice - this.discount);
            this.growthPlanPrice = this.temp;
        }
        else {
            this.annualPlan = false;
            this.soloPlanPrice = this.soloWithoutDiscount;
            this.starterPlanPrice = this.starterWithoutDiscount;
            this.growthPlanPrice = this.growthWithoutDiscount;
        }
    }

}
