import { AuthGardService } from './../auth/auth-gard.service';
import { IsOwnerGuard } from './../auth/is-owner.guard';
import { PaymentComponent } from './payment/payment/payment.component';
import { InviteUserComponent } from './invite-user/invite-user.component';
import { UpgradePlanComponent } from './upgrade-plan/upgrade-plan.component';
import { BillingInfoSettingComponent } from './billing-info-setting/billing-info-setting.component';
import { UserProfileSettingComponent } from './user-profile-setting/user-profile-setting.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './settings.component';

const routes: Routes = [
  {
    path: 'settings', component: SettingsComponent, children: [
      { path: '', redirectTo: 'user-profile', pathMatch: 'full' },
      { path: 'user-profile', component: UserProfileSettingComponent },
      { path: 'billing-info', component: BillingInfoSettingComponent },
      { path: 'upgrade-plan', component: UpgradePlanComponent },
      { path: 'invite-user', component: InviteUserComponent, canActivate: [IsOwnerGuard] },
      { path: 'billing-info/app-payment', component: PaymentComponent }
    ], canActivate: [AuthGardService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeetingsRoutingModule { }

// export const routedComponents = [SettingsComponent];
