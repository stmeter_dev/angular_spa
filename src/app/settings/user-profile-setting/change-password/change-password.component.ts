import { SettingsService } from './../../../services/settings.service';
import { Router } from '@angular/router';
import { AuthService } from './../../../auth/auth.service';
import { ToasterService } from 'angular2-toaster';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { EstimatesService } from './../../../sales/estimates/estimates.service';
import { Http } from '@angular/http';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-change-password',
  templateUrl: 'change-password.component.html',
  styleUrls: ['change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  @Output() changePassword = new EventEmitter();

  constructor(private http: Http,
    public settingsService: SettingsService,
    public toasterService: ToasterService,
    public authService: AuthService,
    public router: Router)
  { }

  ngOnInit() {
    this.initUserPasswordForm();
  }

  initUserPasswordForm() {
    this.form = new FormGroup({
      'current_password': new FormControl(''),
      'password': new FormControl(''),
      'password_confirmation': new FormControl('')
    });
  }

  onCancel() {
    this.initUserPasswordForm();
  }

  onSubmit(form: NgForm) {
    if(!form.valid){
      return;
    }
    this.changePassword.emit(form.value)
    this.settingsService.passwordChange(form.value)
      .subscribe(res => {
        if (res) {
          this.toasterService.pop('success', 'Password Changed', 'You need to login again!');
          this.authService.signout()
            .then(resp => {
              this.router.navigate(['/signin'])
            })
            .catch(err => {
              console.log(err.json())
            });
        }
      }, error => {
        this.toasterService.pop('error', 'Password Change Error', 'You Entered a Wrong Password!');
        this.initUserPasswordForm();
      });
  }
}
