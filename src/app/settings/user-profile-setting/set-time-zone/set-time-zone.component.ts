import { ToasterService } from 'angular2-toaster';
import { SettingsService } from './../../../services/settings.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-set-time-zone',
  templateUrl: 'set-time-zone.component.html',
  styleUrls: ['set-time-zone.component.scss']
})
export class SetTimeZoneComponent {
  user = {};
  timezone: String;
  placeholderString = 'Select timezone';
  showbutton: boolean;
  constructor(private settingsService: SettingsService,
    private toasterService: ToasterService) { }

  changeTimezone(timezone) {
    this.timezone = timezone;
    this.showbutton = true;
  }
  onSetTimeZone(timezone) {
    this.settingsService.setTimeZone(this.timezone)
      .subscribe(res => {
        if (res) {
          this.toasterService.pop('success', 'Timezone Set', 'You have set timezone!');
          this.showbutton = false;
        }
      }, error => {
        this.toasterService.pop('error', 'Timezone Error', 'Someting went wrong!');
      });
  }
}
