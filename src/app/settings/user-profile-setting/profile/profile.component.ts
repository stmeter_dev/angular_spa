import { NgForm, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, Output, EventEmitter, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.scss']
})
export class ProfileComponent implements OnInit, OnChanges {
  @Output() profileFormSubmit = new EventEmitter();
  @Input() name: string;
  form: FormGroup;

  ngOnInit() {
    this.loadUserProfile();
  }

  ngOnChanges() {
    this.loadUserProfile();
  }

  // Init Form with default values
  loadUserProfile() {
    this.form = new FormGroup({
      'name': new FormControl(this.name)
    });
  }

  // On Form Submit
  submit(form: NgForm) {
    this.profileFormSubmit.emit(form.value);
  }

  onCancel() {
    this.loadUserProfile();
  }
}
