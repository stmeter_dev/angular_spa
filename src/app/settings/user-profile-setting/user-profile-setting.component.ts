import { User } from './../../models/user';
import { ToasterService } from 'angular2-toaster';
import { SettingsService } from './../../services/settings.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-profile-setting',
  templateUrl: 'user-profile-setting.component.html',
  styleUrls: ['user-profile-setting.component.scss']
})
export class UserProfileSettingComponent implements OnInit {
  userProfile: any;
  constructor(private settingsService: SettingsService,
              private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.userProfile = this.settingsService.getUser();
  }

  // On Profile Submit
  // To change users name and timezone
  // UserProfile is nothing but user object on rails side
  profileSubmit(value) {
    this.settingsService.changeUserProfile(value).subscribe(userProfile => {
        this.userProfile = userProfile;
        this.toasterService
          .pop('success',
               'Operation Success',
               'User updated successfully');
      }
    );
  }
}
