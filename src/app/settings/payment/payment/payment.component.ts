import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  form: FormGroup;
  constructor(fb: FormBuilder) {
    this.form = fb.group({
      'card_number': [null, Validators.compose([Validators.required, Validators.minLength(14), Validators.maxLength(19)])],
      'cvv_code': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(3)])],
      'exp_month': [null, Validators.required],
      'exp_year': [null, Validators.required],
      'cardholder_name': [null, Validators.required]
    })
  }
  ngOnInit() {
  }

  submitForm(value: any): void {
    console.log('Reactive Form Data: ')
    console.log(value);
  }
}
