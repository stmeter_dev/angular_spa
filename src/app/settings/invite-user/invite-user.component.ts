import { ToasterService } from 'angular2-toaster';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invite-user',
  templateUrl: 'invite-user.component.html',
  styleUrls: ['invite-user.component.scss']
})
export class InviteUserComponent implements OnInit {

  public userData: Object;
  inviteForm: FormGroup;
  isFormSubmit = false;
  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private toasterService: ToasterService
  ) {
    this.inviteForm = this.initForm();
  }

  initForm() {
    const email = '';
    return this.fb.group({
      email: [email, Validators.required]
    });
  }
  ngOnInit() {
    this.userService.getAllUsers().subscribe((data) => {
      this.userData = data.user;
    });
  }

  onSubmit() {
    if (!this.inviteForm.valid) {
      return;
    }
    this.isFormSubmit = true;
    this.userService.inviteUser(this.inviteForm.value).subscribe(response => {
      this.toasterService
        .pop('success', 'Operation success', 'Invitation Sent!!!');
    });
  }
}
