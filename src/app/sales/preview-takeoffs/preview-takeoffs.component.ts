import { Estimate } from './../../models/estimate';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preview-takeoffs',
  templateUrl: './preview-takeoffs.component.html',
  styleUrls: ['./preview-takeoffs.component.css']
})
export class PreviewTakeoffsComponent implements OnInit {

estimates: Estimate[] = [];

  constructor() { }

  ngOnInit() {
  }

}
