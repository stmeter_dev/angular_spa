import { Http } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { Estimate } from '../../models/estimate';
import { EstimatesService } from './estimates.service';
import { AuthService } from '../../auth/auth.service';
import { User } from '../../models/user';
import { Client } from '../../models/client';
import { State } from "clarity-angular";

@Component({
  selector: 'app-estimates',
  templateUrl: './estimates.component.html',
  styleUrls: ['./estimates.component.css']
})
export class EstimatesComponent implements OnInit {
  estimates: Estimate[] = [];
  current_user: User;
  toBeRemoved: Estimate;
  toBeRemovedIndex: number;
  openDeleteDialog: Boolean = false;
  clientData: any;
  estimateData: any;
  pagination: any = {};
  loading: Boolean = false;
  state: State;
  loadSpinner: Boolean = false;
  searchQuery = {
    arrival: 'all',
    status: 'all',
    query: ''
  };
  noData: Boolean = false;

  constructor(
    private toasterService: ToasterService,
    private estimatesService: EstimatesService,
    private authService: AuthService,
    private router: Router

  ) {
    this.current_user = this.authService.getCurrentUser();
  }

  ngOnInit() {
    this.loadEstimates(1);
    this.estimatesService.estimatesState.subscribe(
      (estimates: Estimate[]) => {
        this.estimates = estimates;
      });
    this.estimatesService.estimateUpdated.subscribe(
      (estimate: Estimate) => {
        this.loadEstimates(1);
        this.toasterService.pop('success', 'Operation success', 'Estimate updated successfully')
      }
    );

    this.estimatesService.estimateAdded.subscribe(
      (estimate: Estimate) => {
        this.toasterService.pop('success', 'Operation success', 'Estiamte Created and Synced with Calendar')
      }
    );

    this.router.events.subscribe((val: NavigationEnd) => {
      const re = /code=(.*)/;
      if (val.url.match(re)) {
        console.log('YES', val.url);

        this.fetchEstimateData()
        this.router.navigate(['/sales']);
        this.estimatesService.createClientEstimate(this.clientData, this.estimateData)
          .subscribe(
          (estimate: Estimate) => {
            this.toasterService.pop('success', 'Operation success', 'Estimate Created and Synced with Calendar')
          });
      } else {
        return 0;
      }
    });
  }

  onNewEstimate() {
    this.estimatesService.newEstimate = true;
  }

  onEditEstimate(estimate: Estimate) {
    this.estimatesService.selectedEstimate = estimate;
    this.estimatesService.editEstimateMode = true;
  }

  public onEstimateDeleted(data: any) {
    if (data === true) {
      this.estimatesService.removeEstimate(this.toBeRemoved, this.toBeRemovedIndex)
      this.toasterService.pop('info', 'Estimate Deleted Successfully');
    }
    this.openDeleteDialog = false;
  }

  onDeleteEstimate(estimate: Estimate, index: number) {
    this.openDeleteDialog = true;
    this.toBeRemoved = estimate;
    this.toBeRemovedIndex = index;
    this.refresh(this.state);
  }

  // Load New Estimates from api
  loadEstimates(page: number) {
    this.loadSpinner = true;
    this.estimatesService.fetchEstimates(page)
      .subscribe(
      (resp) => {
        if (!resp['errors']) {
          this.estimates = resp.estimates;
          this.pagination = resp.meta.pagination;
          this.estimatesService.setEstimate(this.estimates);
          this.loadSpinner = false;
        }
      });
  }

  // For New Data in Data Grid
  refresh(state: State) {

    this.loading = true;
    let page = 1;
    if (state && state.page) {
      page = state.page.from ? (state.page.from / 10) + 1 : 1;
    }
    this.loadEstimates(page);
    this.loading = false;

  }

  // For Opening Modals

  isNewEstimate() {
    return this.estimatesService.newEstimate;
  }

  getEditMode() {
    return this.estimatesService.getEstimateEditMode()
  }

  // Search Estimates based on filters and search input by user
  searchEstimates(dataFilter) {

    // if(!dataFilter.value.trim()) { return }
    this.loadSpinner = true;
    this.searchQuery[dataFilter.type] = dataFilter.value;

    this.estimatesService.searchEstimates(this.searchQuery)
      .subscribe((res) => {
        this.estimates = res.estimates;
        this.pagination = res.meta.pagination;
        this.estimatesService.setEstimate(this.estimates);
        this.loadSpinner = false;
      }, (error: Error) => {
        this.loadSpinner = false;
        this.estimates = [];
        this.toasterService
          .pop('error',
          'Operation Failed',
          'Oops!!! Some Error Occured...');
      });
  }
  // status color
  getClassName(status) {
    if (status === 'completed') {
      return `badge badge-1`;
    } else if (status === 'pending') {
      return `badge badge-orange`;
    } else if (status === 'ongoing') {
      return `badge badge-success`;
    }
  }
  private fetchEstimateData() {
    this.clientData = JSON.parse(localStorage.getItem('clientData'))
    this.estimateData = JSON.parse(localStorage.getItem('estimateData'))
  }

}
