import { State } from 'clarity-angular';
import { EstimatesComponent } from './../estimates.component';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { EstimatesService } from '../estimates.service';
import { ClientsService } from '../../../services/clients';
import { Estimate } from '../../../models/estimate';
import { Client } from '../../../models/client';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-new-estimate',
  templateUrl: './new-estimate.component.html',
  styleUrls: ['./new-estimate.component.css']
})
export class NewEstimateComponent implements OnInit {
  windowHandle: any = null;
  clientData: any
  estimateData: any
  workTypes: string[] = ['Interior', 'Exterior', 'Interior & Exterior']
  mask: any[] = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  calendarAuth = new Subject<any>()
  state: State
  checkCredeiantials: boolean;


  constructor(
    private refresh: EstimatesComponent,
    private toasterService: ToasterService,
    private estimatesService: EstimatesService,
    private clientsService: ClientsService,
    private http: Http
  ) { }

  ngOnInit() {
    this.estimatesService.authorizeCalendar().subscribe(
      (res) => {
        console.log('calender call', res);
        if (res.authorized === 'NOK') {
          this.checkCredeiantials = false;
        }
        else {
          this.checkCredeiantials = true;
        }
      });
  }

  onAddEstimate(form: NgForm) {
    this.constructData(form)
    if (this.checkCredeiantials) {
      this.estimatesService.createClientEstimate(this.clientData, this.estimateData)
        .subscribe(
        (estimate: Estimate) => {
          console.log('my esrimated', estimate)
          this.estimatesService.estimateAdded.next(estimate)
          this.estimatesService.addEstimate(new Estimate(estimate))
          this.estimatesService.estimatesState.next(this.estimatesService.getEstimates())
          this.refresh.refresh(this.state);
        }
        )
    }
    this.onCloseModal()
  }

  // On Authorize Click
  onAuthorize() {
    this.estimatesService.authorizeCalendar()
      .subscribe(response => {
        window.location.href = response.redirect_to
      });
  }

  onCloseModal() {
    this.estimatesService.newEstimate = false
  }

  private constructData(form: NgForm) {
    this.clientData = {
      "client": {
        first_name: form.value.firstName,
        last_name: form.value.lastName,
        email: form.value.email,
        phone_number: form.value.phone_number,
        "properties_attributes": [{
          addr_line1: form.value.addr_line1,
          addr_line2: form.value.addr_line2,
          zipcode: form.value.zipcode,
          city: form.value.city
        }]
      }
    }

    this.estimateData = {
      "estimate": {
        property_id: 0,
        status: 'pending',
        start_time: form.value.start_time,
        work_type: form.value.work_type,
        work_description: form.value.work_description
      }
    }

    this.storeData()
  }

  private storeData() {
    localStorage.setItem('clientData', JSON.stringify(this.clientData))
    localStorage.setItem('estimateData', JSON.stringify(this.estimateData))
  }

}
