import { Component, OnInit,OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Estimate } from '../../../models/estimate';
import { EstimatesService } from '../estimates.service';

@Component({
  selector: 'app-edit-estimate',
  templateUrl: './edit-estimate.component.html',
  styleUrls: ['./edit-estimate.component.css']
})
export class EditEstimateComponent implements OnInit {
  estimate: Estimate
  estimateForm: FormGroup
  showStarttime: any
  showWorkDesc: string

  workTypes: string[] = ['Interior', 'Exterior', 'Interior & Exterior']
  constructor(private fb: FormBuilder,
    private estimatesService: EstimatesService) {

  }

  ngOnInit() {
    this.estimate = this.estimatesService.selectedEstimate
    this.createForm()

  }
 
  createForm() {
    /**
     * Note: The Original Enstimate String Comes in format of 'string'+GMT differance 
     * Eg.'timestring+GMT + 5:30' however the input box accespts 
     * only datetimelocal format string i.e 'dd-mm-yyyy,HH:mm'
     * hence the below logic to convert the string as per 
     * the HTML5 datetime format
     * and then creating the form.
     * 
     */
    let isoStr: string;
    let dateTimeLocal: string;

    isoStr = new Date(this.estimate.start_time).toISOString();
    dateTimeLocal = isoStr.substring(0,isoStr.length-1)

    this.estimateForm = this.fb.group({
      start_time: [dateTimeLocal, Validators.required],
      work_type: [this.estimate.work_type, Validators.required],
      work_description: [this.estimate.work_description]
    });
  }

  onSaveChanges(formData: any) {
    this.onCloseModal()
    this.estimatesService.updateEstimate(this.estimate.id, formData)
      .subscribe(
      (estimate: Estimate) => {
        this.estimatesService.estimateUpdated.next(estimate)
        }
      );
  }

  private onCloseModal() {
    this.estimatesService.editEstimateMode = false
  }

}
