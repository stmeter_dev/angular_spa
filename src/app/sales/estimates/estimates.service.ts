import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Estimate } from '../../models/estimate';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../auth/auth.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/mergeMap';
import { Subject } from 'rxjs/Subject';
import { Client } from '../../models/client';



@Injectable()
export class EstimatesService {
  estimates: Estimate[] = []
  selectedEstimate: Estimate
  newEstimate: boolean = false
  editEstimateMode: boolean = false
  estimatesState = new Subject<Estimate[]>()
  estimateUpdated = new Subject<Estimate>()
  estimateAdded = new Subject<Estimate>()
  authGranted = new Subject<string>()



  private apiUrl = environment.apiUrl;
  private headers = new Headers({
    'Content-Type': 'application/json'
  })

  constructor(private http: Http,
    private authService: AuthService) {

  }
  setEstimate(estimates: Estimate[]) {
    this.estimates = estimates
  }

  addEstimate(estimate: Estimate) {
    this.estimates.push(estimate)
  }

  authorizeCalendar() {
    return this.http.get(this.apiUrl + 'oauth2calendarcallback')
      .map((res: Response) => res.json())
  }

  createEstimate(data: any): Observable<any> {
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'estimates',
      JSON.stringify(data),
      { headers: this.headers }
    )
      .map((res: Response) => {
        return res.json()
      })
      .catch(this.handleError)
  }

  createClientEstimate(cData: any, eData: any): Observable<Estimate> {
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'clients',
      JSON.stringify(cData),
      { headers: this.headers }
    )
      .map((res: Response) => res.json().client)
      .mergeMap((client: Client) => {
        eData.estimate.property_id = client.properties[0].id
        return this.http.post(
          this.apiUrl + 'estimates',
          JSON.stringify(eData),
          { headers: this.headers }
        )
          .map((resp: Response) => new Estimate(resp.json().estimate))
      })
      .catch(this.handleError);
  }

  updateEstimate(id: number, estimateData: any): Observable<Estimate> {
    this.editEstimateMode = false;
    this.setHeaders()
    const estimate = this.estimates.find((e: Estimate) => {
      return e.id === id;
    });

    if (estimate) {
      estimate.start_time = estimateData.start_time || estimate.start_time
      estimate.work_type = estimateData.work_type || estimate.work_type
      estimate.work_description = estimateData.work_description || estimate.work_description
      return this.http.put(
        this.apiUrl + 'estimates/' + id,
        JSON.stringify({
          "estimate": {
            "start_time": estimate.start_time,
            "work_type": estimate.work_type,
            "work_description": estimate.work_description
          }
        }),
        { headers: this.headers }
      )
        .map(this.mapEstimate)
        .catch(this.handleError)
    }
  }

  removeEstimate(estimate: Estimate, index: number) {
    return this.http.delete(
      this.apiUrl + '/estimates/' + estimate.id,
      { headers: this.headers }
    )
      .toPromise()
      .then((resp) => {
        this.estimates.splice(index, 1)
        this.estimatesState.next(this.estimates.slice())
      })
      .catch((err) => {
        console.log(err);
      });
  }

  fetchEstimates(page: number): Observable<any> {
    this.setHeaders()
    return this.http.get(
      this.apiUrl + 'estimates?page=' + page,
      { headers: this.headers }
    )
      .map((resp: Response) => {
        //this.estimates = resp.json().estimates
        //this.pagination = resp.json().meta.pagination
        return resp.json()
      })
      .catch((error: Response) => {
        return Observable.of(error.json());
      })
  }

  getEstimates(): Estimate[] {
    return this.estimates.slice()
  }

  getEstimateEditMode() {
    return this.editEstimateMode
  }

  // Search Estimates based on filters and search input by user
  searchEstimates(searchParams: any) {
    this.setHeaders();

    const url = this.apiUrl + 'estimates/search';

    const body = JSON.stringify({ "estimate": searchParams });

    return this.http.post(url, body, { headers: this.headers })
      .map((resp: Response) => {
        return resp.json()
      })
      .catch((error: Response) => {
        return Observable.throw(error)
      })
  }

  private mapClient(res: Response) {
    let client: Client = res.json().client;
    return client
  }

  private mapEstimate(res: Response) {
    let estimate: Estimate = res.json().estimate;
    return estimate
  }

  private handleError(error: Response | any) {
    let errMsg: string = 'Operation failed!!';
    return Observable.throw(errMsg);
  }

  private setHeaders() {
    const user = this.authService.getCurrentUser()
    if (user) {
      this.headers.set('Authorization', user.authToken);
      this.headers.set('X-Account', user.account);
    }
  }

}
