import { AuthGardService } from './../auth/auth-gard.service';
import { PreviewTakeoffsComponent } from './preview-takeoffs/preview-takeoffs.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { SalesComponent } from './sales.component';
import { EstimatesComponent } from './estimates/estimates.component';
import { EstimatingComponent } from './estimating/estimating.component';
import { GlobalRatesComponent } from './estimating/global-rates/global-rates.component';
import { FlexComponent } from './estimating/flex/flex.component';
import { ProdRatesComponent } from './estimating/prodrates/prodrates.component';
import { MaterialsComponent } from './estimating/materials/materials.component';
import { EditFlexRateComponent } from './estimating/edit-flex-rate/edit-flex-rate.component';

const salesRoutes: Routes = [
  {
    path: 'sales',
    component: SalesComponent,
    canActivate: [AuthGardService],
    children: [
      {
        path: '',
        component: EstimatesComponent,
        pathMatch: 'full',
      },
      {
        path: 'estimates',
        component: EstimatesComponent
      },
       {
        path: 'takeoffs',
        component: PreviewTakeoffsComponent,
      },
      {
        path: 'estimating',
        component: EstimatingComponent,
        children: [
          {
            path: '',
            redirectTo: 'grates',
            pathMatch: 'full'
          },
          {
            path: 'grates',
            component: GlobalRatesComponent
          },
          {
            path: 'flex',
            component: FlexComponent
          },
          {
            path: 'prodrates',
            component: ProdRatesComponent
          },
          {
            path: 'materials',
            component: MaterialsComponent
          }
        ]
      }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(salesRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class SalesRoutingModule {}
