import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Toast } from 'angular2-toaster';
import { FlexRate } from '../../../models/flex-rate';
import { EstimatingService } from '../estimating.service';

@Component({
  selector: 'app-edit-flex-rate',
  templateUrl: './edit-flex-rate.component.html',
  styleUrls: ['./edit-flex-rate.component.css']
})
export class EditFlexRateComponent implements OnInit {
  @Output() rateEdited = new EventEmitter<{mode: string, rate: FlexRate}>()
  @Input() isNewRate: boolean
  flexRate: FlexRate
  flexRateForm: FormGroup
  rateUnits: string[] = ['Hr/100 Sf', 'Hr/100 Lf', 'Hr']

  constructor(private fb: FormBuilder,
    private estimatingService: EstimatingService) {

  }

  ngOnInit() {
    this.flexRate = this.estimatingService.selectedFlexRate
    this.createForm()
  }

  createForm() {
    var name: string
    var description: string
    var rate: number
    var unit: string
    if(!this.isNewRate) {
      name = this.flexRate.name
      description = this.flexRate.description
      rate = this.flexRate.rate
      unit = this.flexRate.unit
    }

    this.flexRateForm = this.fb.group({
      name: [name, Validators.required ],
      description: [description ],
      rate: [rate, Validators.required ],
      unit: [unit, Validators.required ]
    })
  }

  onSaveChanges() {
    this.onCloseModal()
    const name = this.flexRateForm.value.name
    const description = this.flexRateForm.value.description
    const rate = this.flexRateForm.value.rate
    const unit = this.flexRateForm.value.unit
    if(this.isNewRate) {
      this.addRate(name, description, rate, unit)
    } else {
      const id = this.flexRate.id
      this.updateRate(id, name, description, rate, unit)
    }
  }

  private updateRate(id: number, name: string,
    description: string, rate: number, unit: string) {

    const updatedFlexRate = new FlexRate(name, description, rate, unit, id)
    this.rateEdited.emit({mode: 'update', rate: updatedFlexRate})
  }

  private addRate(name: string, description: string,
    rate: number, unit: string) {

    const newFlexRate = new FlexRate(name, description, rate, unit)
    this.rateEdited.emit({mode: 'new', rate: newFlexRate})
  }

  private onCloseModal() {
    this.estimatingService.editRateMode = false
  }

}
