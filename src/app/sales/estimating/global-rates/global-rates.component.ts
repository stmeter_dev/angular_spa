import { Response } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { EstimatingService } from '../estimating.service';
import { GlobalRate } from '../../../models/global-rate';

@Component({
  selector: 'app-global-rates',
  templateUrl: './global-rates.component.html',
  styleUrls: ['./global-rates.component.css']
})
export class GlobalRatesComponent implements OnInit {
  globalRates: GlobalRate[] = []
  errorMsg: string
  saveLoading: boolean = false
  form: FormGroup = new FormGroup({});
  payLoad = '';
  constructor(
    private estimatingService: EstimatingService,
    private toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this.loadRates()
  }

  onSubmit(formData: any, valid: boolean) {
    this.form.reset()
    if (valid) {
      for (var i in formData) {
        const rate = this.globalRates.find((rate: GlobalRate) => {
          return rate.id === parseInt(i)
        })
        if (rate.value !== formData[i]) {
          rate.value = formData[i]
          this.estimatingService.updateGlobalRate(rate)
            .subscribe(
            (gRate: GlobalRate) => {
              this.loadRates()
              this.toasterService.pop('success', 'Operation success', 'Rate updated: ' + gRate.name)
            }
            )
        }
      }
    }
  }

  onCancel() {
    this.loadRates()
  }

  private loadRates() {
    this.estimatingService.fetchGlobalRates()
      .subscribe(
      (rates: GlobalRate[]) => {
        this.globalRates = rates
        this.form = this.toFormGroup(this.globalRates)
      },
      (error: Response) => {
        this.errorMsg = error.statusText
      });
  }

  private toFormGroup(rates: GlobalRate[]) {
    let group: any = {};
    rates.forEach(rate => {
      group[rate.id] = new FormControl(rate.value, Validators.required)
    });
    return new FormGroup(group);
  }

}
