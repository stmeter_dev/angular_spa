import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProdRate } from '../../../models/prodrate';
import { EstimatingService } from '../estimating.service';

@Component({
  selector: 'app-edit-prod-rate',
  templateUrl: './edit-prod-rate.component.html',
  styleUrls: ['./edit-prod-rate.component.css']
})
export class EditProdRateComponent implements OnInit {
  @Output() rateEdited = new EventEmitter<{mode: string, rate: ProdRate}>()
  @Input() isNewRate: boolean
  prodRate: ProdRate
  prodRateForm: FormGroup
  rateUnits: string[] = ['Sf/Hr', 'Lf/Hr', 'Hr']

  constructor(private fb: FormBuilder,
    private estimatingService: EstimatingService) {

  }

  ngOnInit() {
    this.prodRate = this.estimatingService.selectedProdRate
    this.createForm()
  }

  createForm() {
    var name: string
    var category: string
    var rate: number
    var unit: string
    if(!this.isNewRate) {
      name = this.prodRate.name
      rate = this.prodRate.rate
      unit = this.prodRate.unit
      category = this.prodRate.category
    }

    this.prodRateForm = this.fb.group({
      name: [name, Validators.required ],
      rate: [rate, Validators.required ],
      unit: [unit, Validators.required ],
      category: [category, Validators.required]
    })
  }

  onSaveChanges() {
    this.onCloseModal()
    const name = this.prodRateForm.value.name
    const category = this.prodRateForm.value.category
    const rate = this.prodRateForm.value.rate
    const unit = this.prodRateForm.value.unit
    if(this.isNewRate) {
      this.addRate(name, rate, unit, category)
    } else {
      const id = this.prodRate.id
      this.updateRate(id, name, rate, unit, category)
    }
  }

  private updateRate(id: number, name: string,
    rate: number, unit: string, category: string) {

    const updatedProdRate = new ProdRate(name, rate, unit, category, id)
    this.rateEdited.emit({mode: 'update', rate: updatedProdRate})
  }

  private addRate(name: string, rate: number,
    unit: string, category: string,) {

    const newProdRate = new ProdRate(name, rate, unit, category)
    this.rateEdited.emit({mode: 'new', rate: newProdRate})
  }

  private onCloseModal() {
    this.estimatingService.editRateMode = false
  }

}
