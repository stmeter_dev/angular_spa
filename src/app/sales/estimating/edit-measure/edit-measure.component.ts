import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Measure } from '../../../models/measure';
import { EstimatingService } from '../estimating.service';

@Component({
  selector: 'app-edit-measure',
  templateUrl: './edit-measure.component.html',
  styleUrls: ['./edit-measure.component.css']
})
export class EditMeasureComponent implements OnInit {
  @Output() measureEdited = new EventEmitter<{mode: string, measure: Measure}>()
  @Input() isNewMeasure: boolean
  measure: Measure
  measureForm: FormGroup
  measureUnits: string[] = ['SF', 'LF']
  measureCategories: string[] = ['Doors', 'Windows', 'Trim Work']

  constructor(private fb: FormBuilder,
    private estimatingService: EstimatingService) {

  }

  ngOnInit() {
    this.measure = this.estimatingService.selectedMeasure
    this.createForm()
  }

  createForm() {
    var name: string
    var category: string
    var value: number
    var unit: string
    if(!this.isNewMeasure) {
      name = this.measure.name
      value = this.measure.value
      unit = this.measure.unit
      category = this.measure.category
    }

    this.measureForm = this.fb.group({
      name: [name, Validators.required ],
      value: [value, Validators.required ],
      unit: [unit, Validators.required ],
      category: [category, Validators.required]
    })
  }

  onSaveChanges() {
    this.onCloseModal()
    const name = this.measureForm.value.name
    const category = this.measureForm.value.category
    const value = this.measureForm.value.value
    const unit = this.measureForm.value.unit
    if(this.isNewMeasure) {
      this.addMeasure(name, value, unit, category)
    } else {
      const id = this.measure.id
      this.updateMeasure(id, name, value, unit, category)
    }
  }

  private updateMeasure(id: number, name: string,
    value: number, unit: string, category: string) {

    const updatedMeasure = new Measure(name, value, unit, category, id)
    this.measureEdited.emit({mode: 'update', measure: updatedMeasure})
  }

  private addMeasure(name: string, value: number,
    unit: string, category: string,) {

    const newMeasure = new Measure(name, value, unit, category)
    this.measureEdited.emit({mode: 'new', measure: newMeasure})
  }

  private onCloseModal() {
    this.estimatingService.editMeasureMode = false
  }

}
