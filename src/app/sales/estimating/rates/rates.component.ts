import { Component, OnInit } from '@angular/core';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EstimatingService } from '../estimating.service';
import { Rate } from '../../../models/rate';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.css']
})
export class RatesComponent implements OnInit {

  rates: Rate[] = []
  toBeRemoved: Rate
  toBeRemovedIndex: number
  openDeleteDialog: boolean = false
  newRate: boolean = false
  errorMsg: string = ''
  private ratesSubscription: Subscription
  constructor(
    private estimatingService: EstimatingService,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {
    this.estimatingService.fetchRates()
    .subscribe(
      (rates: Rate[]) => {
      this.rates = rates
      },
      (error: Response) => {
        this.errorMsg = error.statusText
      }
    )

    this.ratesSubscription = this.estimatingService.ratesState.subscribe(
      (rates: Rate[]) => {
        this.rates = rates
      }
    )
  }

  onEditRate(rate: Rate) {
    this.newRate = false
    this.estimatingService.selectedRate = rate
    this.estimatingService.editRateMode = true
  }

  onNewRate() {
    this.newRate = true
    this.estimatingService.editRateMode = true
    console.log('edit rate mode: ', this.estimatingService.editRateMode)
  }

  onRateEdited(data: {mode: string, rate: Rate}) {
    console.log('onRateEdited called')
    if(data.mode === 'new') {
      this.estimatingService.createRate(data.rate)
      .subscribe(
        (rate: Rate) => {
          console.log(rate)
          this.estimatingService.addRate(rate)
          this.toasterService.pop('success', 'Operation success', 'new Rate created: ' + rate.name)
        },
        (error: Response) => {
          this.toasterService.pop('error', 'Operation failed!')
        }
      )
    } else {
      this.estimatingService.updateRate(data.rate.id, data.rate)
      .subscribe(
        (rate: Rate) => {
          this.estimatingService.ratesState.next(this.estimatingService.getRates())
          this.toasterService.pop('success', 'Operation success', 'Rate updated: ' + data.rate.name)
        },
        (error: Response) => {
          this.toasterService.pop('error', 'Operation failed!')
        }
      )

    }


  }

  onRateDeleted(data: any) {
    if(data === true) {
      this.estimatingService.removeRate(this.toBeRemoved, this.toBeRemovedIndex)
      this.toasterService.pop('info', 'Rated Deleted Successfully');
    }
    this.openDeleteDialog = false
  }

  onDeleteRate(rate: Rate, index: number) {
    this.openDeleteDialog = true
    this.toBeRemoved = rate
    this.toBeRemovedIndex = index
  }

  getEditMode() {
    return this.estimatingService.getFlexRateEditMode()
  }

  ngOnDestroy() {
    this.ratesSubscription.unsubscribe()
  }
}
