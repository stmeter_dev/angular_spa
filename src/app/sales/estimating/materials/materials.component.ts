import { Component, OnInit } from '@angular/core';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { EstimatingService } from '../estimating.service';
import { Material } from '../../../models/material';

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.css']
})
export class MaterialsComponent implements OnInit {
  materials: Material[] = []
  toBeRemoved: Material
  toBeRemovedIndex: number
  newMaterial: boolean = false
  errorMsg: string
  openDeleteDialog: boolean = false
  constructor(
    private estimatingService: EstimatingService,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {
    this.estimatingService.fetchMaterials()
    .subscribe(
      (materials: Material[]) => {
      this.materials = materials
      },
      (error: Response) => {
        this.errorMsg = error.statusText
      }
    )
    this.estimatingService.materialsState.subscribe(
      (materials: Material[]) => {
        this.materials = materials
      }
    )
  }

  onNewMaterial() {
    this.newMaterial = true
    this.estimatingService.editMaterialMode = true
  }

  onEditMaterial(material: Material) {
    this.newMaterial = false
    this.estimatingService.selectedMaterial = material
    this.estimatingService.editMaterialMode = true
  }

  onMaterialEdited(data: {mode: string, material: Material}) {
    console.log('onRateEdited called')
    if(data.mode === 'new') {
      this.estimatingService.createMaterial(data.material)
      .subscribe(
        (material: Material) => {
          this.estimatingService.addMaterial(material)
          this.toasterService.pop('success', 'Operation success', 'new Material created: ' + material.name)
        },
        (error: Response) => {
          this.toasterService.pop('error', 'Operation failed!')
        }
      )
    } else {
      this.estimatingService.updateMaterial(data.material.id, data.material)
      .subscribe(
        (material: Material) => {
          this.estimatingService.materialsState.next(this.estimatingService.getMaterials())
          this.toasterService.pop('success', 'Operation success', 'Material updated: ' + data.material.name)
        },
        (error: Response) => {
          this.toasterService.pop('error', 'Operation failed!')
        }
      )

    }
  }

  onMaterialDeleted(data: any) {
    if(data === true) {
      this.estimatingService.removeMaterial(this.toBeRemoved, this.toBeRemovedIndex)
      this.toasterService.pop('info', 'Material Deleted Successfully');
    }
    this.openDeleteDialog = false
  }

  onDeleteMaterial(material: Material, index: number) {
    this.openDeleteDialog = true
    this.toBeRemoved = material
    this.toBeRemovedIndex = index
    console.log('to be removed index: ', index)
  }

  getEditMode() {
    return this.estimatingService.editMaterialMode
  }

}
