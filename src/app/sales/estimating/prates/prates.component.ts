import { Component, OnInit } from '@angular/core';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { EstimatingService } from '../estimating.service';
import { ProdRate } from '../../../models/prodrate';

@Component({
  selector: 'app-prates',
  templateUrl: './prates.component.html',
  styleUrls: ['./prates.component.css']
})
export class PratesComponent implements OnInit {
  prodRates: ProdRate[] = []
  toBeRemoved: ProdRate
  toBeRemovedIndex: number
  openDeleteDialog: boolean = false
  newRate: boolean = false
  errorMsg: string

  constructor(
    private estimatingService: EstimatingService,
    private toasterService: ToasterService
  ) { }

  ngOnInit() {
    //fetch production rates
    this.estimatingService.fetchProdRates()
    .subscribe(
      (pRates: ProdRate[]) => {
      this.prodRates = pRates
      },
      (error: Response) => {
        this.errorMsg = error.statusText
      }
    )
    this.estimatingService.prodRatesState.subscribe(
      (pRates: ProdRate[]) => {
        this.prodRates = pRates
      }
    )
  }

  onNewRate() {
    this.newRate = true
    this.estimatingService.editRateMode = true
    console.log('edit prod rate mode: ', this.estimatingService.editRateMode)
  }

  onEditRate(rate: ProdRate) {
    this.newRate = false
    this.estimatingService.selectedProdRate = rate
    this.estimatingService.editRateMode = true
  }

  onRateEdited(data: {mode: string, rate: ProdRate}) {
    console.log('onRateEdited called')
    if(data.mode === 'new') {
      this.estimatingService.createProdRate(data.rate)
      .subscribe(
        (rate: ProdRate) => {
          this.estimatingService.addProdRate(rate)
          this.toasterService.pop('success', 'Operation success', 'new Rate created: ' + rate.name)
        },
        (error: Response) => {
          this.toasterService.pop('error', 'Operation failed!')
        }
      )
    } else {
      this.estimatingService.updateProdRate(data.rate.id, data.rate)
      .subscribe(
        (rate: ProdRate) => {
          this.estimatingService.prodRatesState.next(this.estimatingService.getProdRates())
          this.toasterService.pop('success', 'Operation success', 'Rate updated: ' + rate.name)
        },
        (error: Response) => {
          this.toasterService.pop('error', 'Operation failed!')
        }
      )

    }
  }

  onRateDeleted(data: any) {
    if(data === true) {
      this.estimatingService.removeProdRate(this.toBeRemoved, this.toBeRemovedIndex)
      this.toasterService.pop('info', 'Rate Deleted Successfully');
    }
    this.openDeleteDialog = false
  }

  onDeleteRate(rate: ProdRate, index: number) {
    this.openDeleteDialog = true
    this.toBeRemoved = rate
    this.toBeRemovedIndex = index
    console.log('to be removed index: ', index)
  }

  getEditMode() {
    return this.estimatingService.getFlexRateEditMode()
  }

}
