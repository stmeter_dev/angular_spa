import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Toast } from 'angular2-toaster';
import { Rate } from '../../../models/rate';
import { RateCategory } from '../../../models/rate-category';
import { EstimatingService } from '../estimating.service';

@Component({
  selector: 'app-edit-rate',
  templateUrl: './edit-rate.component.html',
  styleUrls: ['./edit-rate.component.css']
})
export class EditRateComponent implements OnInit {
  @Output() rateEdited = new EventEmitter<{mode: string, rate: Rate}>()
  @Input() isNewRate: boolean
  rate: Rate
  rateCategories: RateCategory[] = []
  rateForm: FormGroup
  inoutList: string[] = ['IN', 'EXT']

  constructor(private fb: FormBuilder,
    private estimatingService: EstimatingService) {

  }

  ngOnInit() {
    this.rate = this.estimatingService.selectedRate
    this.rateCategories = this.estimatingService.getRateCategories()
    this.createForm()
  }

  createForm() {
    var name: string
    var first_coat: number
    var second_coat: number
    var description: string
    var rate_category_id: number
    if(!this.isNewRate) {
      name = this.rate.name
      first_coat = this.rate.first_coat
      second_coat = this.rate.second_coat
      description = this.rate.description
      rate_category_id = this.rate.rate_category_id
    }

    this.rateForm = this.fb.group({
      name: [name, Validators.required ],
      description: [description],
      first_coat: [first_coat, Validators.required ],
      second_coat: [second_coat],
      category_id: [rate_category_id, Validators.required ]
    })
  }

  onSaveChanges() {
    this.onCloseModal()
    const name = this.rateForm.value.name
    const first_coat = this.rateForm.value.first_coat
    const second_coat = this.rateForm.value.second_coat
    const description = this.rateForm.value.description
    const rate_category_id = this.rateForm.value.category_id
    if(this.isNewRate) {
      this.addRate(name, first_coat, second_coat, description, rate_category_id)
      console.log('category id',rate_category_id)
    } else {
      const id = this.rate.id
      this.updateRate(id, name, first_coat, second_coat, description, rate_category_id)
    }
  }

  private updateRate(id: number, name: string,
    first_coat: number, second_coat: number, description: string, rate_category_id: number) {

    const updatedRate = new Rate(name, first_coat, second_coat, description, rate_category_id, id)
    this.rateEdited.emit({mode: 'update', rate: updatedRate})
  }

  private addRate(name: string, first_coat: number,
    second_coat: number, description: string, rate_category_id: number) {

    const newRate = new Rate(name, first_coat, second_coat, description, rate_category_id)
    this.rateEdited.emit({mode: 'new', rate: newRate})
  }

  private onCloseModal() {
    this.estimatingService.editRateMode = false
  }

}
