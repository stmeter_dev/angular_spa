import { Component, OnInit } from '@angular/core';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { EstimatingService } from '../estimating.service';
import { Measure } from '../../../models/measure';

@Component({
  selector: 'app-measures',
  templateUrl: './measures.component.html',
  styleUrls: ['./measures.component.css']
})
export class MeasuresComponent implements OnInit {
  measures: Measure[] = []
  toBeRemoved: Measure
  toBeRemovedIndex: number
  openDeleteDialog: boolean = false
  newMeasure: boolean = false
  errorMsg: string
  constructor(
    private estimatingService: EstimatingService,
    private toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.estimatingService.fetchMeasures()
    .subscribe(
      (measures: Measure[]) => {
      this.measures = measures
      },
      (error: Response) => {
        this.errorMsg = error.statusText
      }
    )
    this.estimatingService.measuresState.subscribe(
      (measures: Measure[]) => {
        this.measures = measures
      }
    )
  }

  onNewMeasure() {
    this.newMeasure = true
    this.estimatingService.editMeasureMode = true
    console.log('edit measure mode: ', this.estimatingService.editMeasureMode)
  }

  onEditMeasure(measure: Measure) {
    this.newMeasure = false
    this.estimatingService.selectedMeasure = measure
    this.estimatingService.editMeasureMode = true
  }

  onMeasureEdited(data: {mode: string, measure: Measure}) {
    console.log('onMeasureEdited called')
    if(data.mode === 'new') {
      this.estimatingService.createMeasure(data.measure)
      .subscribe(
        (measure: Measure) => {
          this.estimatingService.addMeasure(measure)
          this.toasterService.pop('success', 'Operation success', 'new Measurement created: ' + measure.name)
        },
        (error: Response) => {
          this.toasterService.pop('error', 'Operation failed!')
        }
      )
    } else {
      this.estimatingService.updateMeasure(data.measure.id, data.measure)
      .subscribe(
        (measure: Measure) => {
          this.estimatingService.measuresState.next(this.estimatingService.getMeasures())
          this.toasterService.pop('success', 'Operation success', 'Measurement updated: ' + measure.name)
        },
        (error: Response) => {
          this.toasterService.pop('error', 'Operation failed!')
        }
      )

    }
  }

  onMeasureDeleted(data: any) {
    if(data === true) {
      this.estimatingService.removeMeasure(this.toBeRemoved, this.toBeRemovedIndex)
      this.toasterService.pop('info', 'Measurement Deleted Successfully');
    }
    this.openDeleteDialog = false
  }


  onDeleteMeasure(measure: Measure, index: number) {
    this.openDeleteDialog = true
    this.toBeRemoved = measure
    this.toBeRemovedIndex = index
    console.log('to be removed index: ', index)
  }

  getMeasureEditMode() {
    return this.estimatingService.getMeasureEditMode()
  }

}
