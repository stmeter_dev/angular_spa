import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EstimatingService } from '../estimating.service';
import { Material } from '../../../models/material';

@Component({
  selector: 'app-edit-material',
  templateUrl: './edit-material.component.html',
  styleUrls: ['./edit-material.component.css']
})
export class EditMaterialComponent implements OnInit {
  @Output() materialEdited = new EventEmitter<{mode: string, material: Material}>()
  @Input() isNewMaterial: boolean
  material: Material
  materialForm: FormGroup

  constructor(private fb: FormBuilder,
    private estimatingService: EstimatingService) {

  }

  ngOnInit() {
    this.material = this.estimatingService.selectedMaterial
    this.createForm()
  }

  createForm() {
    var name: string
    var cov: number
    var cost: number
    if(!this.isNewMaterial) {
      name = this.material.name
      cov = this.material.cov
      cost = this.material.cost
    }

    this.materialForm = this.fb.group({
      name: [name, Validators.required ],
      cov: [cov, Validators.required ],
      cost: [cost, Validators.required ]
    })
  }

  onSaveChanges() {
    this.onCloseModal()
    const name = this.materialForm.value.name
    const cov = this.materialForm.value.cov
    const cost = this.materialForm.value.cost
    if(this.isNewMaterial) {
      this.addMaterial(name, cov, cost)
    } else {
      const id = this.material.id
      this.updateMaterial(id, name, cov, cost)
    }
  }

  private updateMaterial(id: number, name: string,
    cov: number, cost: number) {

    const updatedMaterial = new Material(name, cov, cost, id)
    this.materialEdited.emit({mode: 'update', material: updatedMaterial})
  }

  private addMaterial(name: string, cov: number,
    cost: number) {

    const newMaterial = new Material(name, cov, cost)
    this.materialEdited.emit({mode: 'new', material: newMaterial})
  }

  private onCloseModal() {
    this.estimatingService.editMaterialMode = false
  }

}
