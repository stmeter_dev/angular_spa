import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EstimatingService } from '../estimating.service';
import { RateCategory } from '../../../models/rate-category';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit, OnDestroy {

    rateCategories: RateCategory[] = []
    toBeRemoved: RateCategory
    toBeRemovedIndex: number
    openDeleteDialog: boolean = false
    newCategory: boolean = false
    errorMsg: string = ''
    private categoriesSubscription: Subscription
    constructor(
      private estimatingService: EstimatingService,
      private toasterService: ToasterService
    ) {}

    ngOnInit() {
      this.rateCategories = this.estimatingService.rateCategories
      this.categoriesSubscription = this.estimatingService.rateCategoriesState.subscribe(
        (categories: RateCategory[]) => {
          this.rateCategories = categories
        }
      )
    }

    onEditCategory(categorie: RateCategory) {
      this.newCategory = false
      this.estimatingService.selectedCategory = categorie
      this.estimatingService.editCategoryMode = true
    }

    onNewCategory() {
      this.newCategory = true
      this.estimatingService.editCategoryMode = true
      console.log('edit category mode: ', this.estimatingService.editCategoryMode)
    }

    onCategoryEdited(data: {mode: string, category: RateCategory}) {
      console.log('onCategoryEdited called')
      if(data.mode === 'new') {
        this.estimatingService.createCategory(data.category)
        .subscribe(
          (category: RateCategory) => {
            this.estimatingService.addCategory(category)
            this.toasterService.pop('success', 'Operation success', 'new Rate created: ' + category.name)
          },
          (error: Response) => {
            this.toasterService.pop('error', 'Operation failed!')
          }
        )
      } else {
        this.estimatingService.updateCategory(data.category.id, data.category)
        .subscribe(
          (category: RateCategory) => {
            this.estimatingService.rateCategoriesState.next(this.estimatingService.getRateCategories())
            this.toasterService.pop('success', 'Operation success', 'Category updated: ' + data.category.name)
          },
          (error: Response) => {
            this.toasterService.pop('error', 'Operation failed!')
          }
        )

      }


    }

    onCategoryDeleted(data: any) {
      if(data === true) {
        this.estimatingService.removeCategory(this.toBeRemoved, this.toBeRemovedIndex)
        this.toasterService.pop('info', 'Category Deleted Successfully');
      }
      this.openDeleteDialog = false
    }

    onDeleteCategory(category: RateCategory, index: number) {
      this.openDeleteDialog = true
      this.toBeRemoved = category
      this.toBeRemovedIndex = index
    }

    getEditMode() {
      return this.estimatingService.getEditCategoryMode()
    }

    ngOnDestroy() {
      this.categoriesSubscription.unsubscribe()
    }
}
