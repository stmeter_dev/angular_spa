import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { AuthService } from '../../auth/auth.service';
import { Material } from '../../models/material';
import { FlexRate } from '../../models/flex-rate';
import { ProdRate } from '../../models/prodrate';
import { Rate } from '../../models/rate';
import { RateCategory } from '../../models/rate-category';
import { GlobalRate } from '../../models/global-rate';
import { Measure } from '../../models/measure';


@Injectable()
export class EstimatingService {
  private apiUrl = environment.apiUrl;
  private headers = new Headers({'Content-Type': 'application/json'})
  flexRatesState = new Subject<FlexRate[]>()
  ratesState = new Subject<Rate[]>()
  rateCategoriesState = new Subject<RateCategory[]>()
  prodRatesState = new Subject<ProdRate[]>()
  materialsState = new Subject<Material[]>()
  measuresState = new Subject<Measure[]>()
  selectedFlexRate: FlexRate
  selectedRate: Rate
  selectedCategory: RateCategory
  selectedProdRate: ProdRate
  selectedMaterial: Material
  selectedMeasure: Measure
  editRateMode: boolean = false
  editMaterialMode: boolean = false
  editMeasureMode: boolean = false
  editCategoryMode: boolean = false
  materials: Material[] = []
  flexRates: FlexRate[] = []
  prodRates: ProdRate[] = []
  globalRates: GlobalRate[] = []
  rates: Rate[] = []
  rateCategories: RateCategory[] = []
  measures: Measure[] = []

  constructor(
    private http: Http,
    private authService: AuthService) {}

  fetchMaterials(): Observable<Material[]>  {
    this.setHeaders()
    return this.http.get(
      this.apiUrl + 'materials',
      { headers: this.headers }
    )
    .map((resp: Response) => {
      this.materials = resp.json().materials
      return this.materials
    })
    .catch((error: Response) => {
      return Observable.throw(error)
    })
  }

  createMaterial(newMaterial: Material): Observable<Material> {
    this.editRateMode = false
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'materials/',
      JSON.stringify({
        "material": newMaterial
      }),
      { headers: this.headers }
    )
    .map(this.mapMaterial)
    .catch(this.handleError)
  }

  updateMaterial(id: number, newMaterial: Material): Observable<Material>  {
    this.editRateMode = false
    this.setHeaders()
    const material = this.materials.find((material: Material) => {
      return material.id === id
    })
    if(material) {
      material.name = newMaterial.name || material.name
      material.cov = newMaterial.cov || material.cov
      material.cost = newMaterial.cost || material.cost
      return this.http.put(
        this.apiUrl + 'materials/' + id,
        JSON.stringify({"material": {
          "name": material.name,
          "cov": material.cov,
          "cost": material.cost
          }}),
        { headers: this.headers}
      )
      .map(this.mapMaterial)
      .catch(this.handleError)
    }
  }

  removeMaterial(material: Material, index: number) {
    this.setHeaders()
    this.http.delete(
      this.apiUrl + 'materials/' + material.id,
      { headers: this.headers }
    )
    .toPromise()
    .then((resp) => {
      this.materials.splice(index, 1)
      this.materialsState.next(this.materials.slice())
    })
    .catch((err) => {
      console.log(err)
    })

  }

  fetchGlobalRates(): Observable<GlobalRate[]> {
    this.setHeaders()
    return this.http.get(
      this.apiUrl + 'global_rates',
      { headers: this.headers}
    )
    .map(this.mapGlobalRates)
    .catch(this.handleError)
  }

  updateGlobalRate(newRate: GlobalRate): Observable<GlobalRate> {
    console.log('update gr called')
    this.setHeaders()
    return this.http.put(
      this.apiUrl + 'global_rates/' + newRate.id,
      JSON.stringify({"global_rate": {
        "value": newRate.value
      }}),
      { headers: this.headers}
    )
    .map(this.mapGlobalRate)
    .catch(this.handleError)
  }

  fetchFlexRates(): Observable<FlexRate[]> {
    this.setHeaders()
    return this.http.get(
      this.apiUrl + 'flex_rates',
      { headers: this.headers }
    )
    .map((resp: Response) => {
      this.flexRates = resp.json().flex_rates
      return this.flexRates
    })
    .catch((error: Response) => {
      return Observable.throw(error)
    })
  }

  fetchRates(): Observable<Rate[]> {
    this.setHeaders()
    return this.http.get(
      this.apiUrl + 'rates',
      { headers: this.headers }
    )
    .map((resp: Response) => {
      this.rates = resp.json().rates
      return this.rates
    })
    .catch((error: Response) => {
      return Observable.throw(error)
    })
  }

  fetchRateCategories() {
    this.setHeaders()
    return this.http.get(
      this.apiUrl + 'rate_categories',
      { headers: this.headers }
    )
    .map((resp: Response) => {
      return resp.json().rate_categories
    })
    .subscribe(
      (rates: RateCategory[]) => {
        this.rateCategories = rates
      }
    )
  }

  // fetchRateCategories(): Observable<RateCategory[]> {
  //   this.setHeaders()
  //   return this.http.get(
  //     this.apiUrl + 'rate_categories',
  //     { headers: this.headers }
  //   )
  //   .map((resp: Response) => {
  //     this.rateCategories = resp.json().rate_categories
  //     return this.rateCategories
  //   })
  //   .catch((error: Response) => {
  //     return Observable.throw(error)
  //   })
  // }

  fetchProdRates(): Observable<ProdRate[]> {
    this.setHeaders()
    return this.http.get(
      this.apiUrl + 'production_rates',
      { headers: this.headers }
    )
    .map((resp: Response) => {
      this.prodRates = resp.json().production_rates
      return this.prodRates
    })
    .catch((error: Response) => {
      return Observable.throw(error)
    })
  }

  fetchMeasures(): Observable<Measure[]> {
    this.setHeaders()
    return this.http.get(
      this.apiUrl + 'measures',
      { headers: this.headers }
    )
    .map((resp: Response) => {
      this.measures = resp.json().measures
      return this.measures
    })
    .catch((error: Response) => {
      return Observable.throw(error)
    })
  }

  createMeasure(newMeasure: Measure): Observable<Measure> {
    this.editMeasureMode = false
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'measures/',
      JSON.stringify({
        "measure": newMeasure
      }),
      { headers: this.headers}
    )
    .map(this.mapMeasure)
    .catch(this.handleError)
  }

  updateMeasure(id: number, newMeasure: Measure): Observable<Measure>  {
    this.editMeasureMode = false
    this.setHeaders()
    const measure = this.measures.find((m: Measure) => {
      return m.id === id
    })
    if(measure) {
      measure.name = newMeasure.name || measure.name
      measure.value = newMeasure.value || measure.value
      measure.unit = newMeasure.unit || measure.unit
      measure.category = newMeasure.category || measure.category
      return this.http.put(
        this.apiUrl + 'measures/' + id,
        JSON.stringify({"measure": {
          "name": measure.name,
          "value": measure.value,
          "unit": measure.unit,
          "category": measure.category
          }}),
        { headers: this.headers}
      )
      .map(this.mapMeasure)
      .catch(this.handleError)
    }
  }

  removeMeasure(measure: Measure, index: number) {
    this.setHeaders()
    this.http.delete(
      this.apiUrl + 'measures/' + measure.id,
      { headers: this.headers }
    )
    .toPromise()
    .then((resp) => {
      this.measures.splice(index, 1)
      this.measuresState.next(this.measures.slice())
    })
    .catch((err) => {
      console.log(err)
    })

  }

  updateFlexRate(id: number, newRate: FlexRate): Observable<FlexRate>  {
    this.editRateMode = false
    this.setHeaders()
    const rate = this.flexRates.find((fr: FlexRate) => {
      return fr.id === id
    })
    if(rate) {
      rate.name = newRate.name || rate.name
      rate.description = newRate.description || rate.description
      rate.rate = newRate.rate || rate.rate
      rate.unit = newRate.unit || rate.unit
      return this.http.put(
        this.apiUrl + 'flex_rates/' + id,
        JSON.stringify({"flex_rate": {
          "name": rate.name,
          "description": rate.description,
          "rate": rate.rate,
          "unit": rate.unit
          }}),
        { headers: this.headers}
      )
      .map(this.mapFlexRate)
      .catch(this.handleError)
    }
  }

  updateRate(id: number, newRate: Rate): Observable<Rate>  {
    this.editRateMode = false
    this.setHeaders()
    const rate = this.rates.find((r: Rate) => {
      return r.id === id
    })
    if(rate) {
      rate.name = newRate.name || rate.name
      rate.first_coat = newRate.first_coat || rate.first_coat
      rate.second_coat = newRate.second_coat || rate.second_coat
      rate.description = newRate.description || rate.description
      rate.rate_category_id = newRate.rate_category_id || rate.rate_category_id
      return this.http.put(
        this.apiUrl + 'rates/' + id,
        JSON.stringify({"rate": {
          "name": rate.name,
          "first_coat": rate.first_coat,
          "second_coat": rate.second_coat,
          "description": rate.description,
          "rate_category_id": rate.rate_category_id
          }}),
        { headers: this.headers}
      )
      .map(this.mapRate)
      .catch(this.handleError)
    }
  }

  updateCategory(id: number, newCategory: RateCategory): Observable<RateCategory>  {
    this.editCategoryMode = false
    this.setHeaders()
    const category = this.rateCategories.find((rc: RateCategory) => {
      return rc.id === id
    })
    if(category) {
      category.name = newCategory.name || category.name
      category.unit = newCategory.unit || category.unit
      category.inout = newCategory.inout || category.inout
      return this.http.put(
        this.apiUrl + 'rate_categories/' + id,
        JSON.stringify({"rate_category": {
          "name": category.name,
          "unit": category.unit,
          "inout": category.inout
          }}),
        { headers: this.headers}
      )
      .map(this.mapRateCategory)
      .catch(this.handleError)
    }
  }

  updateProdRate(id: number, newRate: ProdRate): Observable<ProdRate>  {
    this.editRateMode = false
    this.setHeaders()
    const rate = this.prodRates.find((pr: ProdRate) => {
      return pr.id === id
    })
    if(rate) {
      rate.name = newRate.name || rate.name
      rate.rate = newRate.rate || rate.rate
      rate.unit = newRate.unit || rate.unit
      rate.category = newRate.category || rate.category
      return this.http.put(
        this.apiUrl + 'production_rates/' + id,
        JSON.stringify({"production_rate": {
          "name": rate.name,
          "rate": rate.rate,
          "unit": rate.unit,
          "category": rate.category
          }}),
        { headers: this.headers}
      )
      .map(this.mapProdRate)
      .catch(this.handleError)
    }
  }

  createFlexRate(newRate: FlexRate): Observable<FlexRate> {
    this.editRateMode = false
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'flex_rates/',
      JSON.stringify({
        "flex_rate": newRate
      }),
      { headers: this.headers }
    )
    .map(this.mapFlexRate)
    .catch(this.handleError)
  }

  createRate(newRate: Rate): Observable<Rate> {
    this.editRateMode = false
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'rates/',
      JSON.stringify({
        "rate": newRate
      }),
      { headers: this.headers }
    )
    .map(this.mapRate)
    .catch(this.handleError)
  }

  createCategory(newCategory: RateCategory): Observable<RateCategory> {
    this.editCategoryMode = false
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'rate_categories/',
      JSON.stringify({
        "rate_category": newCategory
      }),
      { headers: this.headers }
    )
    .map(this.mapRateCategory)
    .catch(this.handleError)
  }

  createProdRate(newRate : ProdRate): Observable<ProdRate> {
    this.editRateMode = false
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'production_rates/',
      JSON.stringify({
        "production_rate": newRate
      }),
      { headers: this.headers}
    )
    .map(this.mapProdRate)
    .catch(this.handleError)
  }

  addFlexRate(rate: FlexRate) {
    this.flexRates.push(rate)
    this.flexRatesState.next(this.flexRates.slice())
  }

  addRate(rate: Rate) {
    this.rates.push(rate)
    this.ratesState.next(this.rates.slice())
  }

  addCategory(category: RateCategory) {
    this.rateCategories.push(category)
    this.rateCategoriesState.next(this.rateCategories.slice())
  }

  addProdRate(rate: ProdRate) {
    this.prodRates.push(rate)
    this.prodRatesState.next(this.prodRates.slice())
  }

  addMeasure(measure: Measure) {
    this.measures.push(measure)
    this.measuresState.next(this.measures.slice())
  }

  addMaterial(material: Material) {
    this.materials.push(material)
    this.materialsState.next(this.materials.slice())
  }

  removeFlexRate(rate: FlexRate, index: number) {
    this.setHeaders()
    this.http.delete(
      this.apiUrl + 'flex_rates/' + rate.id,
      { headers: this.headers }
    )
    .toPromise()
    .then((resp) => {
      this.flexRates.splice(index, 1)
      this.flexRatesState.next(this.flexRates.slice())
    })
    .catch((err) => {
      console.log(err)
    })

  }

  removeRate(rate: Rate, index: number) {
    this.setHeaders()
    this.http.delete(
      this.apiUrl + 'rates/' + rate.id,
      { headers: this.headers }
    )
    .toPromise()
    .then((resp) => {
      this.rates.splice(index, 1)
      this.ratesState.next(this.rates.slice())
    })
    .catch((err) => {
      console.log(err)
    })

  }

  removeCategory(category: RateCategory, index: number) {
    this.setHeaders()
    this.http.delete(
      this.apiUrl + 'rate_categories/' + category.id,
      { headers: this.headers }
    )
    .toPromise()
    .then((resp) => {
      this.rateCategories.splice(index, 1)
      this.rateCategoriesState.next(this.rateCategories.slice())
    })
    .catch((err) => {
      console.log(err)
    })

  }

  removeProdRate(rate: ProdRate, index: number) {
    this.setHeaders()
    this.http.delete(
      this.apiUrl + 'production_rates/' + rate.id,
      { headers: this.headers }
    )
    .toPromise()
    .then((resp) => {
      this.prodRates.splice(index, 1)
      this.prodRatesState.next(this.prodRates.slice())
    })
    .catch((err) => {
      console.log(err)
    })

  }

  getMaterials() {
    return this.materials.slice();
  }

  getGlobalRates() {
    return this.globalRates;
  }

  getFlexRates() {
    return this.flexRates.slice();
  }

  getRates() {
    return this.rates.slice();
  }

  getRateCategories() {
    return this.rateCategories.slice();
  }

  getProdRates() {
    return this.prodRates.slice();
  }

  getFlexRateEditMode() {
    return this.editRateMode;
  }

  getMeasures() {
    return this.measures.slice();
  }

  getEditCategoryMode() {
    return this.editCategoryMode;
  }

  getMeasureEditMode() {
    return this.editMeasureMode;
  }

  private mapFlexRate(res: Response) {
    let body = res.json().flex_rate;
    let rate: FlexRate = new FlexRate(body.name,
      body.description, body.rate, body.unit, body.id)
    return rate;
  }

  private mapRate(res: Response) {
    let body = res.json().rate;
    let rate: Rate = new Rate(body.name,
      body.first_coat, body.second_coat,
      body.description, body.rate_category_id,
      body.id, body.rate_category)
    return rate;
  }

  private mapRateCategory(res: Response) {
    let body = res.json().rate_category;
    let category: RateCategory = new RateCategory(body.name,
      body.unit, body.inout, body.rates, body.id)
    return category
  }

  private mapProdRate(res: Response) {
    let body = res.json().production_rate;
    let rate: ProdRate = new ProdRate(body.name,
      body.rate, body.unit, body.category, body.id)
    return rate;
  }

  private mapMeasure(res: Response) {
    let body = res.json().measure;
    let measure: Measure = new Measure(body.name,
      body.value, body.unit, body.category, body.id)
    return measure
  }

  private mapMaterial(res: Response) {
    let body = res.json().material;
    let material: Material = new Material(body.name,
      body.cov, body.cost, body.id)
    return material
  }

  private mapGlobalRates(res: Response) {
    let body = res.json().global_rates;
    let globalRates: GlobalRate[] = body
    this.globalRates = globalRates
    return globalRates
  }

  private mapGlobalRate(res: Response) {
    let body = res.json().global_rate;
    let globalRate: GlobalRate = body
    return globalRate
  }

  private handleError (error: Response | any) {
    let errMsg: string = 'Operation failed!!';
    return Observable.throw(errMsg);
  }

  private setHeaders() {
    const user = this.authService.getCurrentUser()
    this.headers.set('Authorization', user.authToken)
    this.headers.set('X-Account', user.account)
  }

}
