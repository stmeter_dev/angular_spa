import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prodrates',
  templateUrl: './prodrates.component.html',
  styleUrls: ['./prodrates.component.css']
})
export class ProdRatesComponent implements OnInit {

  tab1 = true;
  tab2 = false;
  constructor(
  ) { }

  ngOnInit() {

  }

  tabSelect(data: any) {
    if (data.srcElement.id === 'tab1') {
      this.tab1 = true;
      this.tab2 = false;
    } else {
      this.tab1 = false;
      this.tab2 = true;
    }
    console.log(this.tab1, this.tab2);
  }
}
