import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EstimatingService } from '../estimating.service';
import { FlexRate } from '../../../models/flex-rate';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-flex',
  templateUrl: './flex.component.html',
  styleUrls: ['./flex.component.css']
})
export class FlexComponent {
  categoryCount: number
  rateCount: number
  // flexRates: FlexRate[] = []
  // toBeRemoved: FlexRate
  // toBeRemovedIndex: number
  // openDeleteDialog: boolean = false
  // newRate: boolean = false
  // errorMsg: string = ''
  // private fRatesSubscription: Subscription
  // constructor(
  //   private estimatingService: EstimatingService,
  //   private toasterService: ToasterService
  // ) {}
  //
  // ngOnInit() {
  //   this.estimatingService.fetchFlexRates()
  //   .subscribe(
  //     (fRates: FlexRate[]) => {
  //     this.flexRates = fRates
  //     },
  //     (error: Response) => {
  //       this.errorMsg = error.statusText
  //     }
  //   )
  //
  //   this.fRatesSubscription = this.estimatingService.flexRatesState.subscribe(
  //     (fRates: FlexRate[]) => {
  //       this.flexRates = fRates
  //     }
  //   )
  // }
  //
  // onEditRate(rate: FlexRate) {
  //   this.newRate = false
  //   this.estimatingService.selectedFlexRate = rate
  //   this.estimatingService.editRateMode = true
  // }
  //
  // onNewRate() {
  //   this.newRate = true
  //   this.estimatingService.editRateMode = true
  //   console.log('edit flex rate mode: ', this.estimatingService.editRateMode)
  // }
  //
  // onRateEdited(data: {mode: string, rate: FlexRate}) {
  //   console.log('onRateEdited called')
  //   if(data.mode === 'new') {
  //     this.estimatingService.createFlexRate(data.rate)
  //     .subscribe(
  //       (rate: FlexRate) => {
  //         this.estimatingService.addFlexRate(rate)
  //         this.toasterService.pop('success', 'Operation success', 'new Rate created: ' + rate.name)
  //       },
  //       (error: Response) => {
  //         this.toasterService.pop('error', 'Operation failed!')
  //       }
  //     )
  //   } else {
  //     this.estimatingService.updateFlexRate(data.rate.id, data.rate)
  //     .subscribe(
  //       (rate: FlexRate) => {
  //         this.estimatingService.flexRatesState.next(this.estimatingService.getFlexRates())
  //         this.toasterService.pop('success', 'Operation success', 'Rate updated: ' + data.rate.name)
  //       },
  //       (error: Response) => {
  //         this.toasterService.pop('error', 'Operation failed!')
  //       }
  //     )
  //
  //   }
  //
  //
  // }
  //
  // onRateDeleted(data: any) {
  //   if(data === true) {
  //     this.estimatingService.removeFlexRate(this.toBeRemoved, this.toBeRemovedIndex)
  //     this.toasterService.pop('info', 'Rated Deleted Successfully');
  //   }
  //   this.openDeleteDialog = false
  // }
  //
  // onDeleteRate(rate: FlexRate, index: number) {
  //   this.openDeleteDialog = true
  //   this.toBeRemoved = rate
  //   this.toBeRemovedIndex = index
  // }
  //
  // getEditMode() {
  //   return this.estimatingService.getFlexRateEditMode()
  // }
  //
  // ngOnDestroy() {
  //   this.fRatesSubscription.unsubscribe()
  // }


    tab1: boolean = true
    tab2: boolean = false
    constructor(
      private estimatingService: EstimatingService
    ) { }

    ngOnInit() {
      this.estimatingService.fetchRateCategories()
    }

    tabSelect(data:any) {
      if(data.srcElement.id === 'tab1') {
        this.tab1 = true
        this.tab2 = false
      } else {
        this.tab1 = false
        this.tab2 = true
      }
      console.log(this.tab1, this.tab2)
    }
}
