import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Toast } from 'angular2-toaster';
import { RateCategory } from '../../../models/rate-category';
import { EstimatingService } from '../estimating.service';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {
  @Output() categoryEdited = new EventEmitter<{mode: string, category: RateCategory}>()
  @Input() isNewCategory: boolean
  category: RateCategory
  categoryForm: FormGroup
  unitsList: string[] = ['SQF/Hr', 'LF/Hr', 'Hr']
  inoutList: string[] = ['IN', 'EXT']

  constructor(private fb: FormBuilder,
    private estimatingService: EstimatingService) {

  }

  ngOnInit() {
    this.category = this.estimatingService.selectedCategory
    this.createForm()
  }

  createForm() {
    var name: string
    var unit: string
    var inout: string
    if(!this.isNewCategory) {
      name = this.category.name
      unit = this.category.unit
      inout = this.category.inout
    }

    this.categoryForm = this.fb.group({
      name: [name, Validators.required ],
      unit: [unit, Validators.required ],
      inout: [inout, Validators.required ]
    })
  }

  onSaveChanges() {
    this.onCloseModal()
    const name = this.categoryForm.value.name
    const unit = this.categoryForm.value.unit
    const inout = this.categoryForm.value.inout
    if(this.isNewCategory) {
      this.addCategory(name, unit, inout)
    } else {
      const id = this.category.id
      this.updateCategory(id, name, unit, inout)
    }
  }

  private updateCategory(id: number, name: string,
    unit: string, inout: string) {

    const updatedCategory = new RateCategory(name, unit, inout, [], id)
    this.categoryEdited.emit({mode: 'update', category: updatedCategory})
  }

  private addCategory(name: string, unit: string,
    inout: string) {

    const newCategory = new RateCategory(name, unit, inout)
    this.categoryEdited.emit({mode: 'new', category: newCategory})
  }

  private onCloseModal() {
    this.estimatingService.editCategoryMode = false
  }

}
