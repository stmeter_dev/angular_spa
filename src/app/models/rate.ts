import { RateCategory } from './rate-category';
export class Rate {
  constructor(
    public name: string, public first_coat: number, public second_coat: number,
    public description: string, public rate_category_id?: number,
    public id?: number, public rate_category?: RateCategory
  ) {}
}
