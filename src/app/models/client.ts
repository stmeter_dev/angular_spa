import { Property } from './property';

export class Client {
   first_name: string
   last_name: string
   phone_number: string
   email: string
   full_name: string
   id?: number
   properties?: Property[]
  constructor(data: any) {
    this.first_name = data.first_name
    this.last_name = data.last_name
    this.phone_number = data.phone_number
    this.email = data.email
    this.full_name = data.full_name
    this.properties = data.properties
    this.id = data.id
  }
}
