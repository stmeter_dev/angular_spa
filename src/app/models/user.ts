export class User {

  constructor(
    public name: string,
    public email: string,
    public account: string,
    public authToken: string,
    public isAuthenticated: boolean,
    public is_owner: string,
    public id?: string,

  ) { }
}
