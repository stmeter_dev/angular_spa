export class Item {
  constructor(
    public name: string,
    public quantity: number = 1,
    public hours: number,
    public coats: number,
    public prep: number,
    public material: number,
    public comment?: string,
    public picUrl?: string
  ) {}
}