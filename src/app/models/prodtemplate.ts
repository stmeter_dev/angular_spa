import { ProdRate } from './prodrate';

export class ProdTemplate {
  constructor(
    public id: number,
    public name: string,
    public active: boolean,
    public prod_rates: ProdRate[]
  ) {}
}
