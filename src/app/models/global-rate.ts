export class GlobalRate {
  constructor(
    public name: string,
    public value: number,
    public category?: string,
    public id?: number
  ) {}
}
