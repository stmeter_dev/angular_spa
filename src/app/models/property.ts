import { Client } from './client';
export class Property {
  addr_line1: string
  addr_line2: string
  zipcode: string
  city: string
  state: string
  full_address: string
  client?: Client
  id?: number
  constructor(data: any) {
    this.addr_line1 = data.addr_line1
    this.addr_line2 = data.addr_line2
    this.zipcode = data.zipcode
    this.city = data.city
    this.state = data.state
    this.full_address = data.full_address
    this.id = data.id
    //this.address = this.addr_line1 + ', ' + this.addr_line2
    this.client = new Client(data.client)
  }
}
