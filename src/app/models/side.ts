import { Item } from './item';

export class Side {
  constructor(
    public code: string,
    public length: number,
    public height: number,
    public coats: number,
    public items?: Item[]
  ) {}
}