import { Item } from './item';
export class Room {
  constructor (
    public name: string,
    public number: number,
    public width: number,
    public length: number,
    public height: number,
    public type: string,
    public items?: Item[]
  ) {}
}