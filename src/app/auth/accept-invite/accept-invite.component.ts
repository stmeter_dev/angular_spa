import { ToasterService } from 'angular2-toaster';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accept-invite',
  templateUrl: 'accept-invite.component.html',
  styleUrls: ['accept-invite.component.scss']
})
export class AcceptInviteComponent implements OnInit {
  inviteToken: string;
  email: string;
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private toasterService: ToasterService) {
    // Get Inivation Token from params
    this.activatedRoute.queryParams
      .subscribe(params => {
        this.inviteToken = params['invitation_token'];
        this.email = params['email'];
      });
  }

  ngOnInit() {
  }

  onAccept(form) {
    if (!form.valid) {
      return;
    };
    const acceptRequestParams = {
      name: form.value.name,
      password: form.value.password,
      invitation_token: this.inviteToken,
      email: this.email
    };
    this.authService.acceptInvite(acceptRequestParams)
      .subscribe(response => {
        this.toasterService
          .pop('success', 'Operation success', 'Activation Success, Please Login!!!');
          this.router.navigate(['/signin']);
        }, error => {
          this.toasterService.pop('error', 'Operation Failed', 'Activation Failed!!!');
        });
  }
}
