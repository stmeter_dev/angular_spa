import { Location } from '@angular/common';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  email_error: string = null;
  email: string;
  public email_sent = false;
  constructor(
    private authService: AuthService,
    private location: Location
  ) { }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

  submit(form_value, valid) {
    if (!valid) {
      return;
    }
    this.authService.forgotPassword(form_value).subscribe((data) => {
      if (data['error']) {
        this.email_error = data['error'];
        return;
      } else {
        this.email_sent = true;
        this.email_error = null;
      }
    });
  }
}
