import { ToasterService } from 'angular2-toaster';
import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {
  passwordForm: FormGroup;
  isFormSubmit = false;
  token: string;
  email: string;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private toasterService: ToasterService
  ) {
    this.passwordForm = this.initForm();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.email = params['email'];
      this.token = params['reset_password_token'];
    });
  }

  initForm() {
    const password = '';
    const confirmPassword = '';

    return this.fb.group({
      password: [password, Validators.required],
      confirmPassword: [confirmPassword, Validators.required]
    });
  }

  onSubmit() {
    if (!this.passwordForm.valid) {
      return;
    }

    const value = this.passwordForm.value;
    const attributes = {
      user: {
        email: this.email,
        reset_password_token: this.token,
        password: value.password,
        password_confirmation: value.confirmPassword
      }
    };

    this.authService.updatePassword(attributes)
      .subscribe(res => {
        this.toasterService
          .pop('success', 'Operation success', 'Reset Success, Please Login!!!');
        this.router.navigate(['/signin']);
      }, error => {
        this.toasterService.pop('error', 'Operation Failed', 'Rest Failed!!!');
      });
  }
}
