import { ToasterService } from 'angular2-toaster';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  submitLoading: Boolean = false;
  constructor(
    private authService: AuthService,
    private router: Router,
    private toastyService: ToasterService
  ) { }

  ngOnInit() {
  }

  onSignup(form: NgForm) {
    this.submitLoading = true;
    if (!form.valid) {
      this.toastyService.pop('error', 'Operation Failed', 'Please enter valid fields!!!');
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    const password_confirmation = form.value.password_confirmation;
    const account = form.value.account.toLowerCase();
    this.authService.signup(email, password, password_confirmation, account)
      .then(resp => {
        this.submitLoading = false;
        this.router.navigate(['/signin']);
        this.toastyService.pop('success', 'Operation Success', 'SignUp Successfull!!!');
      })
      .catch(err => {
        this.submitLoading = false;
        this.toastyService.pop('error', 'Operation Failed', 'SignUp Failed!!!');
      });
  }
}
