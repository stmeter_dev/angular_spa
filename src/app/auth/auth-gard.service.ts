import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { User } from '../models/user';

@Injectable()
export class AuthGardService implements CanActivate{
  isActive = false;

  constructor(
    private router: Router,
    private authService: AuthService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.authService.isAuth.subscribe(data => {
      this.isActive = data;
      if (!this.isActive) {
        this.router.navigate(['/signin']);
      }
    });
    if (!this.isActive) {
      this.router.navigate(['/signin']);
    }
    return this.isActive;
  }
}
