import { ToasterService } from 'angular2-toaster';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  public email_error: string;
  constructor(
    private authService: AuthService,
    private router: Router,
    private toastService: ToasterService
  ) {
    this.authService.isAuth.subscribe((data) => {
      if (data) {
        this.router.navigate(['']);
      }
    });
  }

  ngOnInit() {
  }

  onSignin(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    this.authService.signin(email, password)
      .subscribe(resp => {
        if (!resp['errors']) {
          this.router.navigate(['/sales']);
        } else {
          this.email_error = resp['errors'];
          this.toastService.pop('error', 'Operation Failed', 'SignIn Failed!!!');
        }
      });
  }

}
