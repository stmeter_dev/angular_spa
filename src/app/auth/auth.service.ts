import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Estimate } from '../models/estimate';
import { Headers, Http } from '@angular/http';
import { User } from '../models/user';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import * as moment from 'moment-timezone';
@Injectable()
export class AuthService {
  private apiUrl = environment.apiUrl;
  private headers = new Headers({ 'Content-Type': 'application/json' });
  current_user: User;
  userState = new Subject<User>();
  timeZone = moment.tz.guess();
  isAuth = new Subject<boolean>();
  constructor(private http: Http) {
  }

  updatePassword(attr: any) {
    const url = this.apiUrl + 'users/change_password_by_reset_token';
    return this.http.post(url, attr, { headers: this.headers }).map(res => res.json());
  }

  signup(email: string, password: string, password_confirmation: string,
    account: string) {
    return this.http.post(
      this.apiUrl + 'accounts',
      JSON.stringify({
        "account": {
          "name": account,
          "time_zone": this.timeZone,
          "owner_attributes": {
            "email": email,
            "password": password,
            "password_confirmation": password_confirmation,
            "time_zone": this.timeZone
          }
        }
      }),
      { headers: this.headers }
    )
      .toPromise();
  }

  signin(email: string, password: string) {
    return this.http.post(
      this.apiUrl + 'sessions',
      JSON.stringify({
        session: {
          email: email,
          password: password
        }
      }),
      { headers: this.headers }
    ).map((data) => {
      this.isAuth.next(true);
      this.handleSuccess(data.json());
      return data.json();
    }).catch((err) => {
      return Observable.of(err.json());
    });
  }

  signout() {
    const user = this.getCurrentUser();
    this.headers.set('X-Account', user.account);
    return this.http.delete(
      this.apiUrl + 'sessions/' + user.authToken,
      { headers: this.headers }
    )
      .toPromise()
      .then(() => {
        this.userState.next(null);
        this.clearUser();
      });
  }

  /**
   * Accept User Invitation
   * @param {any} name
   * @param {any} password
   * @returns {Observable<any>}
   *
   * @memberOf AuthService
   */
  acceptInvite(acceptRequestParams: Object): Observable<any> {
    const params = JSON.stringify({ user: acceptRequestParams });

    return this.http.post(
      this.apiUrl + 'users/accept_invite', params, { headers: this.headers })
      .map((data) => data.json()).catch((err) => {
        return Observable.of(err.json());
      });
  }

  private handleSuccess(data: any) {
    this.current_user = new User(
      data.user.name,
      data.user.email,
      data.account.name,
      data.user.auth_token,
      true,
      data.user.is_owner,
      data.user.id, );
    this.userState.next(this.current_user);
    localStorage.setItem('current_user', JSON.stringify(this.current_user));
  }

  getCurrentUser(): User {
    const user = localStorage.getItem('current_user');
    return JSON.parse(user);
  }

  clearUser() {
    localStorage.removeItem('current_user');
  }

  isAuthenticated() {
    console.log('isauthenticated fired');
    const user = this.getCurrentUser();
    const url = this.apiUrl + `sessions/`;
    this.setHeaders();
    if (user) {
      this.http.get(url + user.authToken, { headers: this.headers }).subscribe((data) => {
        this.isAuth.next(true);
        console.log('is auth set to true', this.isAuth);
      }, (err) => {
        console.log('auth error', err);
        this.isAuth.next(false);
      });
    } else {
      this.isAuth.next(false);
    }
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.json());
  }

  forgotPassword(email: Object) {
    const url = `${this.apiUrl}users/forget_password`;
    const body = email;
    return this.http.post(url, body).map((data) => {
      return data.json();
    }).catch((err) => {
      console.log('error', err);
      return Observable.of(err.json());
    });
  }

  private setHeaders() {
    const user = this.getCurrentUser();
    if (user) {
      this.headers.set('Authorization', user.authToken);
      this.headers.set('X-Account', user.account);
    }
  }
}
