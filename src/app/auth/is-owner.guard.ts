import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { User } from '../models/user';

@Injectable()
export class IsOwnerGuard implements CanActivate {
  isActive = true;
  user: User;
  constructor(
    private router: Router,
    private authService: AuthService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.user = this.authService.getCurrentUser();
    if (!this.user.is_owner) {
      this.isActive = false;
      this.router.navigate(['/settings']);
    }
    return this.isActive;
  }
}
