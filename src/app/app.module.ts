import { IsOwnerGuard } from './auth/is-owner.guard';
import { AcceptInviteComponent } from './auth/accept-invite/accept-invite.component';
import { PaymentComponent } from './settings/payment/payment/payment.component';
import { UserService } from './services/user.service';
import { SettingsService } from './services/settings.service';
import { SetTimeZoneComponent } from './settings/user-profile-setting/set-time-zone/set-time-zone.component';
import { ChangePasswordComponent } from './settings/user-profile-setting/change-password/change-password.component';
import { ProfileComponent } from './settings/user-profile-setting/profile/profile.component';
import { InviteUserComponent } from './settings/invite-user/invite-user.component';
import { UpgradePlanComponent } from './settings/upgrade-plan/upgrade-plan.component';
import { BillingInfoSettingComponent } from './settings/billing-info-setting/billing-info-setting.component';
import { UserProfileSettingComponent } from './settings/user-profile-setting/user-profile-setting.component';
import { SeetingsRoutingModule } from './settings/settings-route';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ClarityModule } from "clarity-angular";
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ToasterModule } from 'angular2-toaster';
import { TextMaskModule } from 'angular2-text-mask';

import { AppComponent } from './app.component';
import { SalesComponent } from './sales/sales.component';
import { OperationsComponent } from './operations/operations.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { EstimatesComponent } from './sales/estimates/estimates.component';
import { NavComponent } from './nav/nav.component';
import { MenuComponent } from './sales/menu/menu.component';
import { AuthService } from './auth/auth.service';
import { EstimatesService } from './sales/estimates/estimates.service';
import { ClientsService } from './services/clients';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SalesRoutingModule } from './sales/sales-routing.module';
import { AppRoutingModule } from './app-routing.module';
import { AuthGardService } from './auth/auth-gard.service';
import { EstimatingService } from './sales/estimating/estimating.service';
import { EstimatingComponent } from './sales/estimating/estimating.component';
import { FlexComponent } from './sales/estimating/flex/flex.component';
import { ProdRatesComponent } from './sales/estimating/prodrates/prodrates.component';
import { GlobalRatesComponent } from './sales/estimating/global-rates/global-rates.component';
import { MaterialsComponent } from './sales/estimating/materials/materials.component';
import { EditRateComponent } from './sales/estimating/edit-rate/edit-rate.component';
import { EditCategoryComponent } from './sales/estimating/edit-category/edit-category.component';
import { EditFlexRateComponent } from './sales/estimating/edit-flex-rate/edit-flex-rate.component';
import { DeleteDialogComponent } from './shared/delete-dialog/delete-dialog.component';
import { EditProdRateComponent } from './sales/estimating/edit-prod-rate/edit-prod-rate.component';
import { EditMaterialComponent } from './sales/estimating/edit-material/edit-material.component';
import { NewEstimateComponent } from './sales/estimates/new-estimate/new-estimate.component';
import { EditEstimateComponent } from './sales/estimates/edit-estimate/edit-estimate.component';
import { EditMeasureComponent } from './sales/estimating/edit-measure/edit-measure.component';
import { PratesComponent } from './sales/estimating/prates/prates.component';
import { MeasuresComponent } from './sales/estimating/measures/measures.component';
import { RatesComponent } from './sales/estimating/rates/rates.component';
import { CategoriesComponent } from './sales/estimating/categories/categories.component';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
// Observables functions
import 'rxjs/add/observable/of';
import { PreviewTakeoffsComponent } from './sales/preview-takeoffs/preview-takeoffs.component';
import { UpdatePasswordComponent } from './auth/update-password/update-password.component';
import { TimezonePickerModule } from 'ng2-timezone-selector';

@NgModule({
  declarations: [
    AppComponent,
    SalesComponent,
    OperationsComponent,
    DashboardComponent,
    SettingsComponent,
    EstimatesComponent,
    NavComponent,
    MenuComponent,
    SigninComponent,
    SignupComponent,
    EstimatingComponent,
    FlexComponent,
    ProdRatesComponent,
    GlobalRatesComponent,
    MaterialsComponent,
    EditFlexRateComponent,
    EditRateComponent,
    EditCategoryComponent,
    DeleteDialogComponent,
    EditProdRateComponent,
    EditMaterialComponent,
    NewEstimateComponent,
    EditEstimateComponent,
    EditMeasureComponent,
    PratesComponent,
    MeasuresComponent,
    RatesComponent,
    CategoriesComponent,
    AuthCallbackComponent,
    ForgotPasswordComponent,
    UserProfileSettingComponent,
    BillingInfoSettingComponent,
    UpgradePlanComponent,
    InviteUserComponent,
    PreviewTakeoffsComponent,
    ProfileComponent,
    ChangePasswordComponent,
    SetTimeZoneComponent,
    UpdatePasswordComponent,
    AcceptInviteComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToasterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    SalesRoutingModule,
    SeetingsRoutingModule,
    ClarityModule.forRoot(),
    AppRoutingModule,
    TextMaskModule,
    TimezonePickerModule
  ],
  providers: [
    AuthService,
    EstimatesService,
    ClientsService,
    AuthGardService,
    IsOwnerGuard,
    EstimatingService,
    SettingsService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
