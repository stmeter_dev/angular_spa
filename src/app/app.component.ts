import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { ToasterConfig } from 'angular2-toaster';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  current_user: User;
  isAuth: boolean;
  activateNav: boolean;
  onSign: boolean;
  public toasterconfig: ToasterConfig = new ToasterConfig({
    showCloseButton: true,
    tapToDismiss: false,
    timeout: 3000
  });

  constructor(
    private authService: AuthService,
    private location: Location,
    private router: Router
  ) {
    this.authService.isAuthenticated();
  }

  ngOnInit() {
    this.current_user = this.authService.getCurrentUser();
    if (this.current_user !== null) {
      this.isAuth = true;
    } else {
      this.isAuth = false;
    }
    this.authService.userState.subscribe(
      (user: User) => {
        this.current_user = user;
        if (this.current_user !== null) {
          this.isAuth = true;
        } else {
          this.isAuth = false;
        }
      }
    );

    this.router.events.subscribe((val) => {
      const path = this.location.path();

      if (path === '/signup') {
        this.onSign = true;
      } else if (path === '/signin') {
        this.onSign = true;
      } else {
        this.onSign = false;
      }

      if (this.isAuth && !this.onSign) {
        this.activateNav = true;
      } else {
        this.activateNav = false;
      }
    })

  }
}
