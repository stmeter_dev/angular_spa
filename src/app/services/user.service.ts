import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './../auth/auth.service';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  private apiUrl = environment.apiUrl;
  private headers = new Headers({
    'Content-Type': 'application/json'
  });
  constructor(
    private http: Http,
    private authService: AuthService) { }

  getAllUsers() {
    this.setHeaders();
    return this.http.get(
      `${this.apiUrl}users`,
      { headers: this.headers}).map((data) => {
      return data.json();
    }).catch((err) => {
      return Observable.of(err);
    })
  }

  inviteUser(email: object) {
    console.log('received email', email);
    const body = email;
    this.setHeaders();

    return this.http.post(
      `${this.apiUrl}users/invite_user`,
      JSON.stringify({
        email: body['email']
      }),
      {headers: this.headers}

    ).map((data) => {
      console.log('invite user', data);
    }).catch((err) => {
      console.log('error invite user');
      return Observable.of(err);
    })
  }

  private setHeaders() {
    const user = this.authService.getCurrentUser();
    if (user) {
      this.headers.set('Authorization', user.authToken);
      this.headers.set('X-Account', user.account);
    }
  }

}
