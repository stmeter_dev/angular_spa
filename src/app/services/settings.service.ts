import { environment } from './../../environments/environment';
import { User } from './../models/user';
import { AuthService } from './../auth/auth.service';
import { Headers, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class SettingsService {
  private apiUrl = environment.apiUrl;
  private headers = new Headers({
    'Content-Type': 'application/json'
  });

  user: User;

  constructor(private http: Http,
    private authService: AuthService) {
    this.user = this.authService.getCurrentUser();
  }

  //Password change
  passwordChange(user_params: any) {
    this.setHeaders();
    const url = this.apiUrl + '/users/update_password/';
    const body = JSON.stringify({ "user": user_params });
    return this.http.post(url, body, { headers: this.headers })
      .map((res) => res.json().user)
  }
  //setting user time zone. 
    setTimeZone(timezone: any) {
    this.setHeaders();
    const url = this.apiUrl + '/users/set_timezone/';
    const body = JSON.stringify({ "user": {"time_zone": timezone }});
    return this.http.post(url, body, { headers: this.headers })
      .map((res) => res.json().user)
  }

  changeUserProfile(value: any) {
    this.setHeaders();
    const url = `${this.apiUrl}/users/${this.user.id}`;
    const queryParams = JSON.stringify({ user: value });
    return this.http.put(url, queryParams, { headers: this.headers })
      .map((res: Response) => res.json().user);
  }

  getUser() {
    return this.user;
  }

  private setHeaders() {
    this.headers.set('Authorization', this.user.authToken);
    this.headers.set('X-Account', this.user.account);
  }
}
