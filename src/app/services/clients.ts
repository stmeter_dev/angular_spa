import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { AuthService } from '../auth/auth.service';
import { User } from '../models/user';
import { Client } from '../models/client';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ClientsService {
  newClient = new Subject<Client>();
  clients: Client[] = [];
  client: Client;
  private apiUrl = environment.apiUrl;
  private headers = new Headers({
    'Content-Type': 'application/json'
  })

  constructor(private http: Http,
    private authService: AuthService) {
  }

  createClient(client: any): Promise<Client> {
    this.setHeaders()
    return this.http.post(
      this.apiUrl + 'clients',
      JSON.stringify(client),
      { headers: this.headers }
    )
    .toPromise()
    .then(res => res.json().client)
    .catch(this.handleError)
  }

  fetchClients(user: User) {
    this.headers.set('Authorization', user.authToken)
    this.headers.set('X-Account', user.account)

    return this.http.get(
      this.apiUrl + 'clients/',
      { headers: this.headers }
    )
    .toPromise()
    .then(res => {
      const list = []
      res.json().clients.forEach((client: Client) => {
        list.push(new Client(client))
      })
      this.clients = list
      return this.clients
    })
    .catch(err => console.log(err))
  }

  fetchClient(id: number, user: User) {
    this.headers.set('Authorization', user.authToken)
    this.headers.set('X-Account', user.account)
    return this.http.get(
      this.apiUrl + '/clients/' + id,
      { headers: this.headers }
    )
    .toPromise()
    .then(res => {
      if (res) {
        this.client = res.json().client;
        return this.client;
      }
    })
    .catch(error => console.log(error))
  }

  private handleError (error: Response | any) {
    const errMsg = 'Operation failed!!';
    return Observable.throw(errMsg);
  }

  private setHeaders() {
    const user = this.authService.getCurrentUser()
    this.headers.set('Authorization', user.authToken)
    this.headers.set('X-Account', user.account)
  }
}
