import { Injectable } from '@angular/core';
import { Room } from '../models/room';
import { Side } from '../models/side';
import { Item } from '../models/item';

@Injectable()
export class TakeoffService {

  roomsList: Room[] = [];
  defaultSideNames: string[] = ['Front', 'Right', 'Back', 'Left'];
  sidesList: Side[] = [];
  sideItems: Item[] = [];

  addRoom(room: Room) {
    this.roomsList.push(room);
  }

  removeRoom(index: number) {
    this.roomsList.splice(index, 1);
  }

  getRooms() {
    return this.roomsList.slice();
  }

  setSides() {
    this.defaultSideNames.forEach((name: string) => {
      this.sidesList.push(new Side(name, 50, 20, 1, this.sideItems))
    });
    console.log(this.sidesList);
  }

  removeSide(index: number) {
    this.sidesList.splice(index, 1);
  }

  getSides() {
    return this.sidesList.slice();
  }
}
