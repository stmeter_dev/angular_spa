import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent implements OnInit {
  @Input() openDialog: boolean
  @Input() element: any
  @Output() itemDeleted = new EventEmitter<any>()
  
  constructor() { }

  ngOnInit() {
  }

  onDelete() {
    this.onCloseDialog()
    this.itemDeleted.emit(true)
  }

  onCloseDialog() {
    this.itemDeleted.emit(false)
  }

}
